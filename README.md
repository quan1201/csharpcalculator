# BASIC CALCULATOR APP IN C#

**Features**

1. Can do 4 basic math operations.
2. Can parse ANY mathematical expressions while preserving their order of operations (brackets -> multiplication, division -> addition, subtraction).
3. Can handle parsing double values.

**Anti-Features**

1. Doesn't do fractions that well and results are not being rounded up correctly
2. The Algo I wrote follows a brute force approach and could use a lot of optimizations. It currently runs in O(n^2) time.
3. Lots of edge case that i might have missed since I am just doing vanilla string-cutting.
4. Does not conduct any error checks and would just terminate when there is an exception so make sure to input it correctly!

**To-do List**

1. Change the Algo such that it runs in O(n) time.
2. Add more mathematic notations like sin(x), exp(x), sqrt(x).


**The UI of the calculator**

![alt-text](Final_UI.PNG)

**Inputting the expression**
 
![alt-text](UITest/Test_5.PNG)

**Calculating the result**

The result is supposed to be 72 but the computer doesn't round doubles up correctly (working on it).

![alt-text](UITest/Res_5.PNG)