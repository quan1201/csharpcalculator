﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caculator
{
    public partial class Form1 : Form
    {
        private HashSet<Char> first; 
        private HashSet<Char> second;
        private HashSet<Char> brackets;

        public Form1()
        {
            InitializeComponent();
            first = new HashSet<Char>();
            second = new HashSet<Char>();
            brackets = new HashSet<Char>();
            first.Add('*');
            first.Add('/');
            second.Add('+');
            second.Add('-');
            brackets.Add('(');
            brackets.Add(')');

        }


        private void num7_Click(object sender, EventArgs e)
        {
            txbResult.Text += "7";
        }

        private void num8_Click(object sender, EventArgs e)
        {
            txbResult.Text += "8";
        }

        private void num9_Click(object sender, EventArgs e)
        {
            txbResult.Text += "9";
        }

        private void num0_Click(object sender, EventArgs e)
        {
            txbResult.Text += "0";
        }

        private void num1_Click(object sender, EventArgs e)
        {
            txbResult.Text += "1"; 
        }

        private void num2_Click(object sender, EventArgs e)
        {
            txbResult.Text += "2";
 
        }

        private void num3_Click(object sender, EventArgs e)
        {
            txbResult.Text += "3";
        }

        private void num5_Click(object sender, EventArgs e)
        {
            txbResult.Text += "5";
        }

        private void num6_Click(object sender, EventArgs e)
        {
            txbResult.Text += "6";
        }

        private void num4_Click(object sender, EventArgs e)
        {
            txbResult.Text += "4";
        }

        private void btnCE_Click(object sender, EventArgs e)
        {
            if (txbResult.Text.Length < 1)
            {
                txbResult.Text = "";
            }
            else {
                txbResult.Text = txbResult.Text.Substring(0, txbResult.Text.Length - 1);
              
            }
            

        }
        private void btnMul_Click(object sender, EventArgs e)
        {
            txbResult.Text += "*";

        }
        private void buttonDiv_Click(object sender, EventArgs e)
        {
            txbResult.Text += "/";
        }

        private void buttonSub_Click(object sender, EventArgs e)
        {
            txbResult.Text += "-";
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            txbResult.Text += "+";
        }
        private void leftBracket_Click(object sender, EventArgs e)
        {
            txbResult.Text += "(";
        }

        private void rightBracket_Click(object sender, EventArgs e)
        {
            txbResult.Text += ")";
        }
        private void btnEqual_Click(object sender, EventArgs e)
        {
            Double result = calculate(txbResult.Text, first, second, brackets);
            txbResult.Text = result.ToString(); 

        }
        private void buttonDot_Click(object sender, EventArgs e)
        {
            txbResult.Text += ".";
        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            txbResult.Text = txbResult.Text.Substring(0, 0);
        }
        private double calculate(String s, HashSet<Char> first, HashSet<Char> second, HashSet<Char> brackets)
        {

            s = calculateBrackets(s, brackets);
            s = calculateFirst(s, first, second);
            s = calculateSecond(s, first, second);
 
            return Math.Round(Double.Parse(s),5);
        }

        private String calculateBrackets(String s, HashSet<Char> brackets)
        {
            while (true)
            {
                bool check = false;
                for (int i = 0; i < s.Length; i++)
                {
                    if (brackets.Contains(s[i])) { check = true; break; }
                }
                if (!check) break;
                for (int i = 0; i < s.Length; i++)
                {
                    if (s[i] == ')')
                    {
                        int k = i-1;
                        StringBuilder bracket = new StringBuilder();
                        while (s[k] != '(')
                        {
                            k--;
                        }
                        bracket.Append(s.Substring(k+1, i-k-1));
                        double tempRes = calculate(bracket.ToString(), first,second,brackets);
                        s = s.Substring(0, k) + tempRes + s.Substring(i+1);
                        break;
                    }
                }
            }
            return s;
        }
        private String calculateFirst(String s, HashSet<Char> first, HashSet<Char> second)
        {
            while(s.Contains('*') || s.Contains("/"))
            {
                for (int i=0;i<s.Length;i++)
                {
                    if (first.Contains(s[i])) {
                        s = formatString(s, i, first, second);
                        break;
                    }
                }

            }
            return s;
        }
        private String calculateSecond(String s, HashSet<Char> first, HashSet<Char> second)
        {
            while (true)
            {
                int index = 0;
                bool check = false;
                for (int i = 0; i < s.Length; i++)
                {
                    if (second.Contains(s[i]))
                    {

                        check = true;
                        index = i;
                    }
                }
                // If the final is a negative numbers then there is only one "-" at the start, index is used to check for the final location of either "+" or "-".
                if (s[0] == '-' && index == 0) { break; }
                if (!check) break;
                for (int i = 0; i < s.Length; i++)
                {
                    //(-20+120) <- ignore the first "-"
                    if (second.Contains(s[i]) && i != 0)
                    {
                        s = formatString(s, i, first, second);
                        break;
                    }
                }

            }
            return s;

        }
        /*
         * This is the most confusing method to remember to I am going to give an example to keep my future self from forgetting
        WE have this "...2*6..." -> Since this method is called once we have reached an operator, we need to get the values on the left and right side of the operator. In this case it would be
        "2" and "6" to calculate the result of the math expression. We obtain "2" and "6", and we feed them along with the operator, which is "index", into the next method to calculate the result
        After doing the calculation, parseString() returns a double value which we used to replace the part of the original string that birthed it. So "12" would replace "2*6". We keep doing this until we only have
        one last number in the original string, that would be the final result of the math expression

        Tricky part is detecting the right X and Y on both sides of the operator to return something. We have to detect a DOUBLE X and DOUBLE Y that work with the current operator and not trample on other operations 
        in the expression.

        Like in "2*-3+1", we need to get "2" and "-3" first to work with '*' and leave "+1" alone. Other annoying edge cases I have briefed in the actual method
         */
        private String formatString(String s, int index, HashSet<Char> first, HashSet<Char> second)
        {
 
            int j = index - 1, i = index + 1;
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            while (j >=0)
            {
                if (first.Contains(s[j]) || second.Contains(s[j]))
                {
                    // Left char is an operator and its a negative sign
                    if (s[j] == '-')
                    {
                        //Example (-6*-6 would fail)
                        if (j == 0) 
                        { 
                            sb1.Append(s[j]); 
                            break; 
                        }
                        // our current value is negative and left to it is an operator -> break and we would cut the string from this leftmost point
                        else if (first.Contains(s[j - 1]) || second.Contains(s[j - 1]))
                        {
                            break;
                        }
                        // "-" acts like the subtraction operator and does not make the value negative
                        else
                        {
                            j++;
                            break;
                        }
                    }
                    // The current char is an operator but not "-" so we break and increment the pointer since we dont want to cut it out
                    else
                    {
                        j++;
                        break;
                    }

                }
                //Append only numbers to our string builder
                sb1.Append(s[j]);
                //Prevents exceptions
                if (j == 0) break;
                //All is good for us to decrement this pointer
                j--;
            }
            //Reverse the string to obtain the real value since we looping left -> right --> reversed number
            Char[] temp = sb1.ToString().ToCharArray();
            Array.Reverse(temp);
            sb1 = new StringBuilder(new string(temp));
           
            bool next = true;
            while (i <=s.Length)
            {
                /* any subtraction sign right to our current operator MUST ONLY make a number negative and CANNOT BE a subtraction so we must include it to the stringbuilder
                 continue to skip the rest of the checks
                 bool is there since there is only supposed to be 1 negative sign, any more than that then the formula becomes invalid (not really, mathematically speaking).
                We only reach this statement once.*/
                if (s[i]=='-' && next && i-index==1)
                {
                    next = false;
                    sb2.Append(s[i]);
                    i++;
                    continue;
                }
                //We have dealt with the annoying case where the right number is negative in the previous block so this is an operator so we dont include that into our stringbuilder
                else if (first.Contains(s[i]) || second.Contains(s[i]))
                {
                    break;
                }
                //Appending only numbers, we dont need to reverse it since we are looping left -> right, preserving the value of the number
                sb2.Append(s[i]);
                i++;
                //Break immediately to prevent annoying exceptions
                if (i == s.Length) break;
            }
 

            Double res = parseString(sb1.ToString(), sb2.ToString(), s[index]);
            return s.Substring(0,j) + res + s.Substring(i);
        }
        private Double parseString(String s1, String s2, char op)
        {
            Console.WriteLine("S1 "+s1);
            Console.WriteLine("S2 "+s2);
            switch (op)
            {

                case '+': return Double.Parse(s1) + Double.Parse(s2);
                case '-': return Double.Parse(s1) - Double.Parse(s2);
                case '*': return Double.Parse(s1) * Double.Parse(s2);
                case '/': return Double.Parse(s1) / Double.Parse(s2);
                default: return -1.0;
            }
        }


    }
}
