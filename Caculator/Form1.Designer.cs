﻿namespace Caculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num7 = new System.Windows.Forms.Button();
            this.num3 = new System.Windows.Forms.Button();
            this.num6 = new System.Windows.Forms.Button();
            this.num9 = new System.Windows.Forms.Button();
            this.buttonDot = new System.Windows.Forms.Button();
            this.num2 = new System.Windows.Forms.Button();
            this.num5 = new System.Windows.Forms.Button();
            this.num8 = new System.Windows.Forms.Button();
            this.buttonDiv = new System.Windows.Forms.Button();
            this.num1 = new System.Windows.Forms.Button();
            this.num4 = new System.Windows.Forms.Button();
            this.num0 = new System.Windows.Forms.Button();
            this.btnCE = new System.Windows.Forms.Button();
            this.btnMul = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonSub = new System.Windows.Forms.Button();
            this.btnEqual = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.txbResult = new System.Windows.Forms.TextBox();
            this.leftBracket = new System.Windows.Forms.Button();
            this.rightBracket = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // num7
            // 
            this.num7.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num7.Location = new System.Drawing.Point(48, 289);
            this.num7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num7.Name = "num7";
            this.num7.Size = new System.Drawing.Size(75, 66);
            this.num7.TabIndex = 0;
            this.num7.Text = "7";
            this.num7.UseVisualStyleBackColor = false;
            this.num7.Click += new System.EventHandler(this.num7_Click);
            // 
            // num3
            // 
            this.num3.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num3.Location = new System.Drawing.Point(277, 458);
            this.num3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num3.Name = "num3";
            this.num3.Size = new System.Drawing.Size(75, 66);
            this.num3.TabIndex = 1;
            this.num3.Text = "3";
            this.num3.UseVisualStyleBackColor = false;
            this.num3.Click += new System.EventHandler(this.num3_Click);
            // 
            // num6
            // 
            this.num6.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num6.Location = new System.Drawing.Point(277, 374);
            this.num6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num6.Name = "num6";
            this.num6.Size = new System.Drawing.Size(75, 66);
            this.num6.TabIndex = 2;
            this.num6.Text = "6";
            this.num6.UseVisualStyleBackColor = false;
            this.num6.Click += new System.EventHandler(this.num6_Click);
            // 
            // num9
            // 
            this.num9.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num9.Location = new System.Drawing.Point(277, 289);
            this.num9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num9.Name = "num9";
            this.num9.Size = new System.Drawing.Size(75, 66);
            this.num9.TabIndex = 3;
            this.num9.Text = "9";
            this.num9.UseVisualStyleBackColor = false;
            this.num9.Click += new System.EventHandler(this.num9_Click);
            // 
            // buttonDot
            // 
            this.buttonDot.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonDot.Location = new System.Drawing.Point(277, 542);
            this.buttonDot.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDot.Name = "buttonDot";
            this.buttonDot.Size = new System.Drawing.Size(75, 66);
            this.buttonDot.TabIndex = 4;
            this.buttonDot.Text = ".";
            this.buttonDot.UseVisualStyleBackColor = false;
            this.buttonDot.Click += new System.EventHandler(this.buttonDot_Click);
            // 
            // num2
            // 
            this.num2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num2.Location = new System.Drawing.Point(164, 458);
            this.num2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(75, 66);
            this.num2.TabIndex = 5;
            this.num2.Text = "2";
            this.num2.UseVisualStyleBackColor = false;
            this.num2.Click += new System.EventHandler(this.num2_Click);
            // 
            // num5
            // 
            this.num5.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num5.Location = new System.Drawing.Point(164, 374);
            this.num5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num5.Name = "num5";
            this.num5.Size = new System.Drawing.Size(75, 66);
            this.num5.TabIndex = 6;
            this.num5.Text = "5";
            this.num5.UseVisualStyleBackColor = false;
            this.num5.Click += new System.EventHandler(this.num5_Click);
            // 
            // num8
            // 
            this.num8.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num8.Location = new System.Drawing.Point(164, 289);
            this.num8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num8.Name = "num8";
            this.num8.Size = new System.Drawing.Size(75, 66);
            this.num8.TabIndex = 7;
            this.num8.Text = "8";
            this.num8.UseVisualStyleBackColor = false;
            this.num8.Click += new System.EventHandler(this.num8_Click);
            // 
            // buttonDiv
            // 
            this.buttonDiv.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonDiv.Location = new System.Drawing.Point(397, 286);
            this.buttonDiv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDiv.Name = "buttonDiv";
            this.buttonDiv.Size = new System.Drawing.Size(75, 66);
            this.buttonDiv.TabIndex = 8;
            this.buttonDiv.Text = "/";
            this.buttonDiv.UseVisualStyleBackColor = false;
            this.buttonDiv.Click += new System.EventHandler(this.buttonDiv_Click);
            // 
            // num1
            // 
            this.num1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num1.Location = new System.Drawing.Point(48, 458);
            this.num1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num1.Name = "num1";
            this.num1.Size = new System.Drawing.Size(75, 66);
            this.num1.TabIndex = 9;
            this.num1.Text = "1";
            this.num1.UseVisualStyleBackColor = false;
            this.num1.Click += new System.EventHandler(this.num1_Click);
            // 
            // num4
            // 
            this.num4.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num4.Location = new System.Drawing.Point(48, 374);
            this.num4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num4.Name = "num4";
            this.num4.Size = new System.Drawing.Size(75, 66);
            this.num4.TabIndex = 10;
            this.num4.Text = "4";
            this.num4.UseVisualStyleBackColor = false;
            this.num4.Click += new System.EventHandler(this.num4_Click);
            // 
            // num0
            // 
            this.num0.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.num0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.num0.Location = new System.Drawing.Point(48, 545);
            this.num0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num0.Name = "num0";
            this.num0.Size = new System.Drawing.Size(191, 66);
            this.num0.TabIndex = 11;
            this.num0.Text = "0";
            this.num0.UseVisualStyleBackColor = false;
            this.num0.Click += new System.EventHandler(this.num0_Click);
            // 
            // btnCE
            // 
            this.btnCE.BackColor = System.Drawing.Color.Red;
            this.btnCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnCE.Location = new System.Drawing.Point(48, 632);
            this.btnCE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCE.Name = "btnCE";
            this.btnCE.Size = new System.Drawing.Size(75, 66);
            this.btnCE.TabIndex = 12;
            this.btnCE.Text = "<==";
            this.btnCE.UseVisualStyleBackColor = false;
            this.btnCE.Click += new System.EventHandler(this.btnCE_Click);
            // 
            // btnMul
            // 
            this.btnMul.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnMul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnMul.Location = new System.Drawing.Point(397, 374);
            this.btnMul.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMul.Name = "btnMul";
            this.btnMul.Size = new System.Drawing.Size(75, 66);
            this.btnMul.TabIndex = 13;
            this.btnMul.Text = "*";
            this.btnMul.UseVisualStyleBackColor = false;
            this.btnMul.Click += new System.EventHandler(this.btnMul_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.Red;
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonClear.Location = new System.Drawing.Point(164, 632);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 66);
            this.buttonClear.TabIndex = 14;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonSub
            // 
            this.buttonSub.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonSub.Location = new System.Drawing.Point(397, 458);
            this.buttonSub.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSub.Name = "buttonSub";
            this.buttonSub.Size = new System.Drawing.Size(75, 66);
            this.buttonSub.TabIndex = 15;
            this.buttonSub.Text = "-";
            this.buttonSub.UseVisualStyleBackColor = false;
            this.buttonSub.Click += new System.EventHandler(this.buttonSub_Click);
            // 
            // btnEqual
            // 
            this.btnEqual.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnEqual.Location = new System.Drawing.Point(527, 286);
            this.btnEqual.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEqual.Name = "btnEqual";
            this.btnEqual.Size = new System.Drawing.Size(75, 154);
            this.btnEqual.TabIndex = 16;
            this.btnEqual.Text = "=";
            this.btnEqual.UseVisualStyleBackColor = false;
            this.btnEqual.Click += new System.EventHandler(this.btnEqual_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonAdd.Location = new System.Drawing.Point(397, 542);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 66);
            this.buttonAdd.TabIndex = 18;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // txbResult
            // 
            this.txbResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.txbResult.Location = new System.Drawing.Point(48, 66);
            this.txbResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbResult.Multiline = true;
            this.txbResult.Name = "txbResult";
            this.txbResult.Size = new System.Drawing.Size(554, 184);
            this.txbResult.TabIndex = 19;
            // 
            // leftBracket
            // 
            this.leftBracket.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.leftBracket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.leftBracket.Location = new System.Drawing.Point(277, 634);
            this.leftBracket.Margin = new System.Windows.Forms.Padding(4);
            this.leftBracket.Name = "leftBracket";
            this.leftBracket.Size = new System.Drawing.Size(75, 64);
            this.leftBracket.TabIndex = 20;
            this.leftBracket.Text = "(";
            this.leftBracket.UseVisualStyleBackColor = false;
            this.leftBracket.Click += new System.EventHandler(this.leftBracket_Click);
            // 
            // rightBracket
            // 
            this.rightBracket.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.rightBracket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.rightBracket.Location = new System.Drawing.Point(397, 634);
            this.rightBracket.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rightBracket.Name = "rightBracket";
            this.rightBracket.Size = new System.Drawing.Size(75, 64);
            this.rightBracket.TabIndex = 21;
            this.rightBracket.Text = ")";
            this.rightBracket.UseVisualStyleBackColor = false;
            this.rightBracket.Click += new System.EventHandler(this.rightBracket_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 727);
            this.Controls.Add(this.rightBracket);
            this.Controls.Add(this.leftBracket);
            this.Controls.Add(this.txbResult);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.btnEqual);
            this.Controls.Add(this.buttonSub);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.btnMul);
            this.Controls.Add(this.btnCE);
            this.Controls.Add(this.num0);
            this.Controls.Add(this.num4);
            this.Controls.Add(this.num1);
            this.Controls.Add(this.buttonDiv);
            this.Controls.Add(this.num8);
            this.Controls.Add(this.num5);
            this.Controls.Add(this.num2);
            this.Controls.Add(this.buttonDot);
            this.Controls.Add(this.num9);
            this.Controls.Add(this.num6);
            this.Controls.Add(this.num3);
            this.Controls.Add(this.num7);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button num7;
        private System.Windows.Forms.Button num3;
        private System.Windows.Forms.Button num6;
        private System.Windows.Forms.Button num9;
        private System.Windows.Forms.Button buttonDot;
        private System.Windows.Forms.Button num2;
        private System.Windows.Forms.Button num5;
        private System.Windows.Forms.Button num8;
        private System.Windows.Forms.Button buttonDiv;
        private System.Windows.Forms.Button num1;
        private System.Windows.Forms.Button num4;
        private System.Windows.Forms.Button num0;
        private System.Windows.Forms.Button btnCE;
        private System.Windows.Forms.Button btnMul;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonSub;
        private System.Windows.Forms.Button btnEqual;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox txbResult;
        private System.Windows.Forms.Button leftBracket;
        private System.Windows.Forms.Button rightBracket;
    }
}

